$(document).ready(function () {
    "use strict"
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    //Gán sự kiện cho nút đăng nhập
    $("#btn-login").on("click", function () {
        onBtnLoginClick();
    });
    //Gán sự kiện đăng ký
    $("#register").on("click", function () {
        onBtnRegisterClick();
    });
    //Gán sự kiện cho nút đồng ý tạo tài khoản
    $("#btn-accept-register").on("click", function () {
        onBtnAcceptRegisterClick();
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    onPageLoading();
    //Hàm xử lý sự kiện cho nút login
    function onBtnLoginClick() {
        //B1:Tạo object để thu thập dữ liệu
        var vLoginData = {
            username: "",
            password: ""
        };
        //B2: Thu thập dữ liệu
        getData(vLoginData);
        //B3: Kiểm tra dữ liệu 
        if (isValidateData(vLoginData)) {
            //B4:Gọi sever
            $.ajax({
                url: "http://localhost:8090/login",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(vLoginData),
                success: function (res) {
                    console.log("Đăng nhập thành công");
                    console.log(res);
                },
                error: function (err) {
                    alert("Tên đăng nhập hoặc mật khẩu không chính xác");
                    console.log(err);
                }
            })
        };
    };
    //Hàm xử lý sự kiện tạo tài khoản
    function onBtnRegisterClick() {
        $("#modal-register").modal("show");
    }
    //Hàm xử lý sự kiện cho nút đồng ý đăng ký
    function onBtnAcceptRegisterClick() {
        //B1: Tạo object
        var vRegisterData = {
            username: "",
            password: "",
        }
        //B2: Thu thập dữ liệu
        getDataRegister(vRegisterData);
        //B3: Kiểm tra dữ liệu 
        if (isValidateDataRegister(vRegisterData)) {
            //B4: Gọi server
            $.ajax({
                url:"http://localhost:8090/register",
                type:"POST",
                data:JSON.stringify(vRegisterData),
                contentType: "application/json",
                success:function(res){
                    alert("Tạo tài khoản thành công");
                    console.log(res);
                },
                error: function(err){
                    alert("Tài khoản đã tồn tại");
                }
            })
        }
    };
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //Hàm tải trang
    function onPageLoading() {
        $.ajax({
            url: "http://localhost:8090/users",
            type: "GET",
            success: function (res) {
                console.log(res);
            },
            error: function (err) {
                console.log(err);
            }
        })
    }
    //Hàm thu thập dữ liệu
    function getData(paramObject) {
        paramObject.username = $("#inp-username").val().trim();
        paramObject.password = $("#inp-password").val().trim();
    }
    //Hàm kiểm tra dữ liệu
    function isValidateData(paramObject) {
        if (paramObject.username == "") {
            alert("Hãy nhập tên đăng nhập");
            return false;
        };
        if (paramObject.password == "") {
            alert("Hãy nhập mật khẩu");
            return false;
        };
        return true;
    }
    //Hàm thu thập dữ liệu để đăng ký
    function getDataRegister(paramObject) {
        paramObject.username = $("#inp-register-username").val().trim();
        paramObject.password = $("#inp-register-password").val().trim();
    }
    //Hàm kiểm tra dữ liệu 
    function isValidateDataRegister(paramObject) {
        var vComfirmPassword = $("#inp-comfirm-register-password").val().trim();
        if (paramObject.username == "") {
            alert("Hãy nhập tên đăng nhập");
            return false;
        };
        if (paramObject.password == "") {
            alert("Hãy nhập mật khẩu");
            return false;
        };
        if(paramObject.password !== vComfirmPassword){
            alert("Mật khẩu không trùng khớp");
            return false;
        }
        return true;
    }
});